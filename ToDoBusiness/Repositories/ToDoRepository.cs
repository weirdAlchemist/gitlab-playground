﻿using System;
using ToDoModels;

namespace ToDoBusiness.Repositories
{
    public class ToDoRepository
    {
        public ToDoRepository()
        {
        }

        public ToDoList CreateList(string name)
        {
            var list = new ToDoList() {
                Items = new System.Collections.Generic.List<ToDoItem>(),
                Description = name
            };

            return list;
        }

        public ToDoList LoadList(string name)
        {
            //ToDo: db?
            return CreateList(name);
        }

        public bool SaveList(ToDoList list)
        {
            //ToDo:db?
            return list.IsValid();
        }
    }
}
