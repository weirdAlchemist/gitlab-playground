﻿using System;
namespace ToDoModels
{
    public class ToDoItem
    { 

        public string Description { get; set; }
        public bool IsDone { get; set; }
        public DateTime? DueDate { get; set; }


        public ToDoItem()
        {

        }

        public bool IsValid()
        {
            return !String.IsNullOrEmpty(Description);
        }
    }
}
