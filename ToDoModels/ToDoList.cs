﻿using System;
using System.Collections.Generic;

namespace ToDoModels
{
    public class ToDoList
    {
        public string Description { get; set; }
        public List<ToDoItem> Items { get; set; }

        public ToDoList()
        {

        }

        public bool IsValid()
        {
            return !String.IsNullOrEmpty(Description) && Items != null;
        }
    }
}
