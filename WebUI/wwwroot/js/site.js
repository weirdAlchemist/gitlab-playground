﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function GetData() {
    $.ajax({
        url: "Home/GetDataView",
        success: function (data) {
            console.log(data);
            $("#con").html(data);
        },
        failure: function (data, status, xhr) {}
    });
}